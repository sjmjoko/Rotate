#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include<bitset>
#include<vector>
#include<array>
#include<algorithm>
using namespace std;

#define ROWS 16
#define COLUMNS 56
#define PC_2_COLS 48
template < class T >
void print(vector<T> v)
{
	for (int i = 0; i < v.size(); i++)
	{
		cout << v[i] << " ";
	}
	cout << endl;
}
template< class X >
void print2D(vector< vector<X>> x)
{
	for (int i = 0; i < x.size(); i++)
	{
		for (int j = 0; j < x[0].size(); j++)
		{
			cout << x[i][j] << " ";
		}
		cout << endl;
	}

}
int main()
{
	vector<vector<int> > tempKey,encryptionKey; //2D vector that stores the 16 keys
	vector<int> v, w, x;  //temprary vectors for Ci,Di and Ki
	int  roundBit[] = { 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1 }; //number of bits to be shited
	int PC_2_Table[] = {14,17,11,24,1,5,3,28,15,6,21,10,23,19,12,4,26,8,16,7,27,
		                 20,13,2,41,52,31,37,47,55,30,40,51,45,33,48,44,49,39,56,
	                    34,53,46,42,50,36,29,32};
	

	for (int i = 1; i < 29; i++)
	{
		v.push_back(i);
	}
	for (int i = 29; i < 57; i++)
	{
		w.push_back(i);
	}
	encryptionKey.resize(ROWS);
	for (int i = 0; i < ROWS; ++i)
	{
		encryptionKey[i].resize(PC_2_COLS);   //allocate space for columns
	}
	tempKey.resize(ROWS);    //allocate space for rows
	for (int i = 0; i < ROWS; ++i)
	{
		tempKey[i].resize(COLUMNS);   //allocate space for columns
	}
	x.reserve(v.size() + w.size());
	for (int i = 0; i < 16; i++)
	{
		rotate(v.begin(), v.begin() + roundBit[i], v.end());
		rotate(w.begin(), w.begin() + roundBit[i], w.end());
		x.insert(x.end(), v.begin(), v.end());
		x.insert(x.end(), w.begin(), w.end());
		for (int j = 0; j < 56; j++)
		{
			tempKey[i][j] = x[j];
		}
		x.clear();
	}
//	print2D(tempKey);
	for (int i = 0; i < ROWS; i++)
	{
		for (int j = 0; j < PC_2_COLS; j++)
		{
			encryptionKey[i][j] = tempKey[i][PC_2_Table[j]-1];
		}
	}
	print2D(encryptionKey);
	cout << endl;
	system("pause");
	return 0;
}